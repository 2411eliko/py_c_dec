from subprocess import Popen, PIPE
from os import path, mkdir, remove
from ctypes import CDLL
from distutils.spawn import find_executable


__all__ = ['c_function', 'c_code', 'c_compile']

COMPILERS = ['gcc', 'cc']


def get_compiler():
    """ Checking if cc/gcc (mainly for Windows machine) are installed.
    """
    for compiler in COMPILERS:
        res = find_executable(compiler)
        if find_executable(compiler):
            return res


COMPILER = get_compiler()

if not COMPILER:
    raise OSError('gcc or cc must be installed..')

C_PATH = './c_funcs'

if not path.exists(C_PATH):
    mkdir(C_PATH)


def exist(c_file, so_file, code) -> bool:
    """ Checking if the C code is already exists without
    changing. in this case, the file will not be re-compile.
    """
    if path.exists(c_file) and path.exists(so_file):
        with open(c_file, 'r') as f:
            if f.read() == code:
                return True
    return False


def create_so_file(c_file, so_file, code):
    """ Compiling the files using command:
        '$ cc -fPIC -shared -o  so_file  c_file'
    """
    with open(c_file, 'w') as file:
        file.write(code)

    _code = [
        COMPILER, '-fPIC', '-shared', '-o', so_file, c_file
    ]

    p = Popen(
        _code,
        stdout=PIPE,
        stdin=PIPE,
        stderr=PIPE,
        close_fds=True
    ).communicate()

    err = p[1].decode('utf-8')

    if err:
        remove(c_file)

        raise SyntaxError(err)


def c_compile(func_name: str, code: str) -> callable:
    file_path = path.join(C_PATH, func_name)
    c_file = file_path + '.c'
    so_file = file_path + '.so'

    if not exist(c_file, so_file, code):
        create_so_file(c_file, so_file, code)
    return CDLL(so_file)


def c_code(func):
    def call():
        return c_compile(func.__name__, func.__doc__)

    return call()


def c_function(func):
    def call():
        return c_compile(func.__name__, func.__doc__)

    return getattr(call(), func.__name__)
