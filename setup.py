from setuptools import setup, find_packages

with open("README.md", "r") as r:
    long_description = r.read()


setup(
    name='run_c',
    version='0.0.1',
    description='call c functions using decorators',
    url="https://gitlab.com/2411eliko/py_c_dec",
    long_description=long_description,
    long_description_content_type='text/markdown',
    author='https://t.me/eliko2411',
    packages=find_packages(),
)
