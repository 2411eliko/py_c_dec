Hi,

Welcome to the fastest way to run C code in your Python code.  
  
This module allows you to run C code (Ctypes) within your Python script.  
  
It works by allowing you to add the decorator to a function, and then writing your C code as a docstring to that function.  See examples below.  Enjoy!
  
  
**Requirements :**  

The compilation is done using cc on Linux.  

* In order to work with a Windows machine, a gcc  compiler must be installed  
(it can be installed with [**cygwin**](https://cygwin.com/packages/summary/gcc-core.html)).
  
**Installation :**  

```bash
pip install git+https://gitlab.com/2411eliko/py_c_dec.git --user
```
Or:
```bash
git clone https://gitlab.com/2411eliko/py_c_dec.git
cd py_c_dec
python setup.py install --user
```
   
  
  
Using:  
 `c_function` decorator:
```python
from run_c import c_function


@c_function
def func():
    r"""
    #include <stdio.h>

    void func(){
        printf("hello world\n");
    }
    """


func()

# result 'hi you'

```

or with parameters (you don't have to add the `hint type`..):

```python

@c_function
def add(x: int, y: int) -> int:
    r"""

    int add(int x, int y){
        return x + y;
    }
    """


print(add(5, 3))

# result: 8
```

You can also create multiple functions, using the `c_code` decorator, which return an object that has all the functions:

```python
from run_c import c_code


@c_code
def functions():
    r"""
    #include <stdio.h>

    void hello_world(){
        printf("hello world\n");
    }

    int lower(int x, int y){
        if (x < y)
            return x;
        return y;
    }

    void func(){
        hello_world();
        printf("%d", lower(4, 5));
    }

    """


print(functions.lower(5, 3))
functions.hello_world()
functions.func()

# results: 
# 4
# 'hello world'
# 
# 'hello world'
# 3
```

If you use the `c_function` decorator, there must be a function in the C code with the same name as the Python function, 
and this function will be called.  
  
You can create some internal functions called by that function.

```python
from run_c import c_function


@c_function
def add_two():
    r"""

    int add_one(int x) {
       return x+1;
    }

    int add_two(int y) {
       return add_one(add_one(y));
    }
    """


print(add_two(3)) 
# result: 5
```
It is recommended to use r-string (r""" """) to avoid errors caused by "\n". and so...
  
  
  
If you want to pass str as a parameter you need to decode it:

```python
from run_c import c_function

@c_function
def rev():
    r"""
    #include <stdio.h>
    #include <string.h>
    
    void reverse(char*, int, int);
    
    void rev(char *x){
       reverse(x, 0, strlen(x)-1);
    
       printf("%s\n", x);
    }
    
    void reverse(char *x, int begin, int end){
       char c;
    
       if (begin >= end)
          return;
    
       c = *(x+begin);
       *(x+begin) = *(x+end);
       *(x+end) = c;
    
       reverse(x, ++begin, --end);
    }
    """


rev('eliko'.encode()) # .encode('utf-8')

# result : okile
```